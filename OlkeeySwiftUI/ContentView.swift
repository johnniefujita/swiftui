//
//  ContentView.swift
//  OlkeeySwiftUI
//
//  Created by johnnie fujita on 18/02/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
